﻿using UnityEngine;
using System.Collections;

public class PresentControl : MonoBehaviour {

    // Use this for initialization

    private Transform myTrans;
    private BaseScript baseControl;
    private GameObject myBase;
	void Start () {
        myTrans = gameObject.GetComponent<Transform>();
        myBase = GameObject.FindGameObjectWithTag("grotto");
        baseControl = myBase.GetComponent<BaseScript>();
	}
	
	// Update is called once per frame
	void Update () {
        myTrans.Rotate(new Vector3(0, 1, 0) * Time.deltaTime);
	
	}

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag == "player")
        {
            baseControl.AddjustCurrentHealth(-2000);
            Destroy(gameObject);
        }
    }
}
