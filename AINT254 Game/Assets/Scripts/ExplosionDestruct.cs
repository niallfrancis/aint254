﻿using UnityEngine;
using System.Collections;

public class ExplosionDestruct : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Invoke("DestroySelf", 3f);
	}


    void DestroySelf()
    {
        Destroy(gameObject);
    }
}
