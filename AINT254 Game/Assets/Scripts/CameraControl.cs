﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {

    private const float yAngleMin = 20.0f;
    private const float yAngleMax = 20.0f;

    public Transform lookAt;
    public Transform camTransform;

    public GameObject pauseMenu;
   

    private Camera cam;

    private Vector3 relativePos;

    public bool paused = false;
    public bool mouse = false;
    private float distance = 10.0f;
    private float currentX = 0.0f;
    private float currentY = 0.0f;
    private float sensX = 4.0f;
    private float sensY = 1.0f;
    private Vector3 velocity = Vector3.zero;

    private void Start()
    {
        camTransform = transform;
        cam = Camera.main;

    }

    private void Update()
    {

        if (!paused)
        {
            currentX += Input.GetAxis("Mouse X") * sensX;
            currentY += Input.GetAxis("Mouse Y") * sensY;

            currentY = Mathf.Clamp(currentY, yAngleMin, yAngleMax);
        }
            
        
        

        //camTransform.LookAt(lookAt.position);
        //camTransform.Translate(Vector3.right * Time.deltaTime * Input.GetAxis("MouseX"));

        if (!mouse)
        {
            Cursor.lockState = CursorLockMode.Confined; // keep confined in the game window
            Cursor.lockState = CursorLockMode.Locked;   // keep confined to center of screen
            Cursor.visible = false;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None; // keep confined in the game window
            Cursor.lockState = CursorLockMode.None;   // keep confined to center of screen
            Cursor.visible = true;
        }
        

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;
            mouse = !mouse;

            if (paused)
            {
                
                pauseMenu.SetActive(true);
                Time.timeScale = 0.0f;
            }
            else
            {
                
                pauseMenu.SetActive(false);
                Time.timeScale = 1.0f;
                
                
            }
        }
        

    }

    private void LateUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rot = Quaternion.Euler(currentY, currentX, 0);
        //camTransform.position = lookAt.position + rot * dir;
        camTransform.LookAt(lookAt.position);



        camTransform.position = Vector3.SmoothDamp(camTransform.position, lookAt.position + rot * dir, ref velocity, 0.1f);
      
    }
}
