﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIControl : MonoBehaviour {

    public int level;
    public int score;

    private WaveController myController;

    public Text scoreText;
    public Text levelText;

    


	// Use this for initialization
	void Start () {
        

        myController = gameObject.GetComponent<WaveController>();

        score = myController.score;
        level = myController.level;
	}
	
	// Update is called once per frame
	void Update () {

        scoreText.text = "Score: " + score;
        levelText.text = "Level: " + level;

        score = myController.score;
        level = myController.level;
	}

    public void ExitGame()
    {
        Application.Quit();
    }
    public void LoadGame()
    {
        Application.LoadLevel(1);
    }
}
