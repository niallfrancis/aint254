﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Gun : MonoBehaviour
{
    public GameObject ball;
    private AudioSource shootSound;
    private Transform m_transform;
    public bool paused = false;

    public int pooledAmount = 25;
    List<GameObject> bullets;


    // Use this for initialization
    void Start()
    {
        m_transform = transform;
        bullets = new List<GameObject>();
        shootSound = gameObject.GetComponent<AudioSource>();

        for (int i = 0; i < pooledAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(ball);
            obj.SetActive(false);
            bullets.Add(obj);
        }
    }

    // Update is called once per frame
    void Update()
    {


        if (!paused)
        {
            if (Input.GetMouseButtonDown(0))
            {
                for (int i = 0; i < bullets.Count; i++)
                {
                    if (!bullets[i].activeInHierarchy)
                    {
                        bullets[i].transform.position = m_transform.position;
                        bullets[i].transform.rotation = m_transform.rotation;
                        bullets[i].SetActive(true);
                        shootSound.Play();
                        break;

                    }
                }

            }
        }




        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;


        }








        
    }
}
