﻿using UnityEngine;
using System.Collections;

public class CarScript : MonoBehaviour {

    private float speed = 20.0f;
    private float rotationSpeed = 175.0f;
    private Rigidbody myRigidBody;

    private void Start()
    {
        myRigidBody = gameObject.GetComponent<Rigidbody>();
    }

    private void Update()
    {
        float translation = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed * Time.deltaTime;

        myRigidBody.velocity = new Vector3(0, 0, 0);
        myRigidBody.angularVelocity = new Vector3(0, 0, 0);
        transform.Translate(0, 0, translation);
        if (Input.GetAxis("Vertical") < -0.1)
        {
            transform.Rotate(0, rotation * -1, 0);
        }
        else
        {
            transform.Rotate(0, rotation, 0);
        }
        
    }
}
