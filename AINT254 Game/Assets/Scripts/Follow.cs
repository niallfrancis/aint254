﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {


    public GameObject target;
    private Transform myTransform;
    private Transform targetTransform;
    private float distance;

	// Use this for initialization
	void Start () {

        targetTransform = target.transform;
        myTransform = gameObject.transform;

	}
	
	// Update is called once per frame
	void Update () {
        myTransform.position = targetTransform.position;
	}
}
