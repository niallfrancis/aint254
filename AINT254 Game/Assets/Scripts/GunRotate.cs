﻿using UnityEngine;
using System.Collections;

public class GunRotate : MonoBehaviour {

    
    private Transform myTransform;
    public bool paused;
    private float currentX = 0.0f;
    

	// Use this for initialization
	void Start () {
        myTransform = gameObject.transform;
        paused = false;
	}
	
	// Update is called once per frame
	void LateUpdate () {

        if (!paused)
        {
            currentX += Input.GetAxis("Mouse X") * 4.0f;

            myTransform.rotation = Quaternion.AngleAxis(currentX, Vector3.up);
        }
        



        if (Input.GetKeyDown(KeyCode.Escape))
        {
            paused = !paused;

            
        }


	}


}
