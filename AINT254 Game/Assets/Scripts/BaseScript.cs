﻿using UnityEngine;
using System.Collections;

public class BaseScript : MonoBehaviour {

    public CameraControl camControl;
    public Gun myGun;
    public GunRotate myGunRot;
    public GameObject loseScreen;
    public int maxHealth = 100000;
    public int curHealth = 100000;
    public Texture2D bgImage;
    public Texture2D fgImage;

    public float healthBarLength;
    private AudioSource alarmSound;

    // Use this for initialization
    void Start()
    {
        Time.timeScale = 1.0f;
        alarmSound = gameObject.GetComponent<AudioSource>();
        healthBarLength = Screen.width / 2;
    }

    // Update is called once per frame
    void Update()
    {
        AddjustCurrentHealth(0);
        
        
    }

    void OnGUI()
    {

        
        
        GUI.BeginGroup(new Rect(Screen.width /4, 10, healthBarLength, 32));
        
        GUI.Box(new Rect(0, 0, healthBarLength, 32), bgImage);

        
        GUI.BeginGroup(new Rect(0, 0, curHealth / maxHealth * healthBarLength, 32));
        
        GUI.Box(new Rect(0, 0, healthBarLength, 32), fgImage);
        
        GUI.EndGroup();

        GUI.EndGroup();
    }

    public void AddjustCurrentHealth(int adj)
    {

        curHealth -= adj;

        if (curHealth < 0)
        {
            curHealth = 0;
            camControl.mouse = true;
            camControl.paused = true;
            myGun.paused = true;
            myGunRot.paused = true;
            Time.timeScale = 0.0f;
            loseScreen.SetActive(true);
        }
            

        if (curHealth > maxHealth)
            curHealth = maxHealth;

        if (maxHealth < 1)
            maxHealth = 1;

        healthBarLength = (Screen.width / 2) * (curHealth / (float)maxHealth);

    }

    public void OnCollisionStay(Collision col)
    {
        if (col.gameObject.tag == "enemy")
        {
            
            AddjustCurrentHealth(10);
        }
    }

    public void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "enemy")
        {
            
            if (!alarmSound.isPlaying)
            {
                alarmSound.Play();
            }
        }
    }

   
   

}
