﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemySpawner : MonoBehaviour
{

    public GameObject enemy;
    private Transform myTrans;
    private float max;
    private float min;
    private int enemyStartAmount;
    private int enemyLeftAmount;

    private int enemyAmount = 25;
    List<GameObject> enemies;

    // Use this for initialization
    void Start()
    {

        max = 65;
        min = -65;

        
        myTrans = gameObject.transform;




        enemies = new List<GameObject>();

        for (int i = 0; i < enemyAmount; i++)
        {
            GameObject obj = (GameObject)Instantiate(enemy);
            obj.SetActive(false);            
            enemies.Add(obj);
        }

        


    }

    // Update is called once per frame
    void Update()
    {


    }

    public void setEnemyAmount(int startingAmount)
    {
        enemyStartAmount = startingAmount;
        enemyLeftAmount = enemyStartAmount;
    }
   

    public void SpawnEnemies()
    {
        
            if (enemyLeftAmount > 0)
            {
                for (int j = 0; j < enemies.Count; j++)
                {
                    if (!enemies[j].activeInHierarchy && !enemies[j + 1].activeInHierarchy)
                    {
                        
                        enemies[j].transform.position = new Vector3(Random.Range(min, max), 0, myTrans.position.z);
                        enemies[j].transform.rotation = Quaternion.Euler(0, 0, 0);
                        enemies[j].transform.localScale = new Vector3(1, 2, 1);
                        enemies[j + 1].transform.position = new Vector3(myTrans.position.x, 0, Random.Range(min, max));
                        enemies[j + 1].transform.rotation = Quaternion.Euler(0, 0, 0);
                        enemies[j + 1].transform.localScale = new Vector3(1, 2, 1);
                        enemies[j].SetActive(true);
                        enemies[j + 1].SetActive(true);
                        enemyLeftAmount -= 4;
                        break;
                    }
                }
                Invoke("SpawnEnemies", Random.Range(2,6));
            }
            
            
            




        
        
    }
}
