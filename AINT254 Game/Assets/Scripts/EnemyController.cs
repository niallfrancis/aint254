﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

    public GameObject present;
    private float speed = 5.0f;
    private bool fresh;
    private AudioSource deathSound;
    private Rigidbody myRigid;
    private int health = 1;
    private Vector3 moveSpeed;
    private WaveController myController;
    private BoxCollider myCol;
	// Use this for initialization
	void Start () {
        myController = FindObjectOfType<WaveController>();
        deathSound = gameObject.GetComponent<AudioSource>();
        myRigid = gameObject.GetComponent<Rigidbody>();
        health = myController.health;
        myCol = gameObject.GetComponent<BoxCollider>();
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {

        if (fresh == true)
        {
            myCol.enabled = true;
            health = myController.health;
            fresh = false;
        }
       moveSpeed = new Vector3 (0,0,0) - gameObject.transform.position;

       myRigid.velocity = moveSpeed.normalized * speed;
       

        //transform.GetComponent<NavMeshAgent>().destination = new Vector3(0, 0, 0);

      
	}   

    void OnEnable()
    {
        transform.LookAt(new Vector3(0, 0, 0));
        fresh = true;
        
        
    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "bullet" || col.gameObject.tag == "player")
        {
            health--;
            if (health == 0)
            {
                myController.killedNumber++;
                myController.score += 20;
                StartCoroutine(ScaleDown(0.4f));
                deathSound.Play();
                //Spawn health bonus aproxx 1/20 times
                if (Random.value < 0.07)
                {
                    Instantiate(present, new Vector3(myRigid.transform.position.x, 0.66f, myRigid.transform.position.z), myRigid.rotation);
                }
                myCol.enabled = false;
                //Invoke("KillSnowman", 0.4f);
                
            }
            
        }
    }

    IEnumerator ScaleDown(float scaleTime)
    {
        Vector3 originalScale = myRigid.transform.localScale;
        Vector3 targetScale = new Vector3(0.1f, 0.1f, 0.1f);

        float currentTime = 0.0f;

        do 
        {
            myRigid.transform.localScale = Vector3.Lerp(originalScale,targetScale,currentTime / scaleTime);
            currentTime += Time.deltaTime;
            yield return null;
        } while (currentTime <= scaleTime);
        
        gameObject.SetActive(false);

    }


    void KillSnowman()
    {
        gameObject.SetActive(false);
        
    }
}
