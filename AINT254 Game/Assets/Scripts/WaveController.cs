﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class WaveController : MonoBehaviour {

    public Text myText;
    public Image myPanel;
    public int level = 0;
    public int score = 0;
    public int health = 1;
    public int enemyNumber;
    public int killedNumber;
    public EnemySpawner mySpawn1;
    public EnemySpawner mySpawn2;




	// Use this for initialization
	void Start () {
        myText.enabled = false;
        myPanel.enabled = false;
        
	}
	
	// Update is called once per frame
	void Update () {
        if (killedNumber == enemyNumber)
        {
            Invoke("NextLevel", 5);
            if (level != 0)
            {
                myPanel.enabled = true;
                myText.enabled = true;
            }
            
        }
	}


   public void NextLevel()
    {
        CancelInvoke();
        level++;
        killedNumber = 0;
        myText.enabled = false;
        myPanel.enabled = false;

        if (level == 1)
        {
            enemyNumber = 4;
        }
        if (level % 3 == 0)
        {
            enemyNumber += 4;
        }
        if (level % 6 == 0 && health < 3)
        {
            health += 1;
        }


        //if (level == 1 || level == 2)
        //{
        //    enemyNumber = 12;
        //}
        //else if (level >= 3 && level <= 5)
        //{
        //    enemyNumber = 16;
        //}
        //else if (level >= 6 && level <= 8)
        //{
        //    enemyNumber = 20;
        //}
        //else if (level >= 9)
        //{
        //    enemyNumber = 24;
        //}
        mySpawn1.setEnemyAmount(enemyNumber);
        mySpawn1.SpawnEnemies();
        mySpawn2.setEnemyAmount(enemyNumber);
        mySpawn2.SpawnEnemies();
        
        
    }
}
