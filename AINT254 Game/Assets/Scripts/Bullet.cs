﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
    private float force = 200;
    private Rigidbody m_rigidbody;
    private Transform m_transform;

	// Use this for initialization
	void OnEnable() 
    {
        Invoke("Destroy", 2.0f);
        m_rigidbody = GetComponent<Rigidbody>();
        m_transform = GetComponent<Transform>();
        m_rigidbody.AddForce(m_transform.forward * force, ForceMode.Impulse);

        
	}

    void Destroy()
    {
        m_rigidbody.velocity = Vector3.zero;
        m_rigidbody.angularVelocity = Vector3.zero;
        gameObject.SetActive(false);

    }

    void OnDisable()
    {
        CancelInvoke();
    }
}
