﻿using UnityEngine;
using System.Collections;

using System.Collections.Generic;

public class BulletHit : MonoBehaviour {

    public GameObject explosion;
    private Rigidbody m_rigidbody;

    void Start()
    {
        m_rigidbody = gameObject.GetComponent<Rigidbody>();

        
        

        
    }
    void OnCollisionEnter(Collision col)
    {      
        if (col.gameObject.tag != "player")
        {
            
            m_rigidbody.velocity = Vector3.zero;
            m_rigidbody.angularVelocity = Vector3.zero;

            Instantiate(explosion,m_rigidbody.position,m_rigidbody.rotation);


            gameObject.SetActive(false);
        }       
            
        
    }

   
}
