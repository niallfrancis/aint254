﻿using UnityEngine;
using System.Collections;

public class LoadSceneOnClick : MonoBehaviour {

    public void LoadByIdex(int sceneIndex)
    {
        Application.LoadLevel(sceneIndex);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
